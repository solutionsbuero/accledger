# accledger

Creates the ledger entries for a given Acc dataset.


## Modules

The library distinguishes between different cases to create the correct ledger entries.

- [ ] Invoice (`Invoice`)
- [ ] Revoked Invoice (`InvoiceRevoked`)
- [ ] Expense paid with Debit Card (`ExpenseDebitCard`)
- [ ] Expense paid with Credit Card (`ExpenseCreditCard`)
- [ ] Expense paid with Bank Transfer (`ExpenseBankTransfer`)
- [ ] Expense paid with Cash (`ExpenseCash`)
- [ ] Advanced Expense settled by Credit Card (`AdvancedExpenseCreditCard`)
- [ ] Advanced Expense settled by Bank Transfer (`AdvancedExpenseBankTransfer`)
- [ ] Advanced Expense settled by Cash (`AdvancedExpenseCash`)
- [ ] Credit Card expenses (`CreditCardSettlement`)

If possible

- [ ] Monthly Salary (`MonthlySalary`)
